<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProcedimientoController extends Controller
{
    public function desarrollo(Request $request){

        
        //Conversiones
        $volumen_c = $request->volumen * 1000;
        $dio_total_c = $request->dio_total / 100;
        $dio_letal_c = $request->dio_letal / 100;
        $tiempo_c = $request->tiempo * 60;

        $c = (-$dio_total_c*$volumen_c);
        
        $x = $dio_total_c * $volumen_c + $c*exp(-$request->litros/$volumen_c*$tiempo_c);

        $respuesta = $x / $volumen_c;

        if($respuesta > $dio_letal_c){
            return redirect('/')->with('error','SE MUERE');
        }else{
            return redirect('/')->with('success','VIVE');
        }
        


    }


}
