<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proyecto mezclas</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>
    <body>
        
        <div class="container">

                <h6 for="">Un estudiante de cuarto de Ingeniería Industrial decide poner
                        fin a su vida porque no entiende las ecuaciones diferenciales lineales de primer
                        orden. Para ello construye un dispositivo que consta de (a) una conducción que
                        comunica el tubo de escape de su coche con el habitáculo interior y (b) una
                        bomba que extrae aire del interior del vehículo y lo expulsa al exterior. Una
                        vez que el coche se pone en marcha, la conducción introduce en el vehículo
                        un 80 % de monóxido de carbono CO (ce = 0,8) a una velocidad de re = 1
                        litros de aire por segundo) y una bomba extrae aire del interior a la misma
                        velocidad. El volumen del habitáculo interior es de V0 = 3 m3 y se admite que
                        en todo momento el CO se distribuye de forma homogénea por el habitáculo.
                        El coche es blindado y ha sido trucado para no poder abrirse desde dentro y
                        para que el motor no pueda apagarse una vez arrancado. Al iniciar el proceso
                        de suicidio, el estudiante se arrepiente y, tras desesperados y fallidos intentos
                        por salir del coche, recuerda que guarda un tefono móvil en la guantera, el
                        cual usa para llamar a su madre. Si una proporción de un 5 % de monóxido
                        de carbono es letal y la madre tarda 5 minutos en llegar, ¿consigue salvarse
                        nuestro estudiante?.
                        Solución: una concentración del 5 % se alcanza en 193.6 segundos (es decir,
                        algo más de tres minutos). “Mala suerte”.</h6>

        </div>
<div class="container center-block">
    
    {!! Form::open(['route' => 'prueba', 'method' => 'post']) !!}
    <h4><label for=""> Ingresar los siguientes datos (PORCENTAJE): </label></h4>
                        
    <div class="row">
    <div class="col-sm-3">
        <table>
            
            <tr>
                <td><input type="text" name="dio_total" placeholder="Dióxido total"></td>
                
            </tr>
            <tr>
                <td><input type="text" name="" placeholder="C" disabled></td>
            </tr>
            <tr>
                <td><input type="text" name="litros" placeholder="Litros de aire por segundo" ></td>
            </tr>
        </table>
    </div>
    <div class="col-sm-6">
        <table>
            <tr>
                <td><input type="text" name="" placeholder="X(t)" disabled></td>
            </tr>
            <tr>
                <td><input type="text" name="dio_letal" placeholder="Dióxido letal"></td>
            </tr>
            <tr>
                <td><input type="text" name="tiempo" placeholder="Tiempo (minutos)"></td>
            </tr>
            <tr>
                <td><input type="text" name="volumen" placeholder="Volumen habitácula"></td>
            </tr>
        </table>
    </div>
</div>

    <button type="submit" class="btn btn-primary"> Calcular </button>

    {!! Form::close() !!}
    </div>
    @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error')}}
        </div>
    @elseif (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success')}}
        </div>
    @endif
    </body>
</html>
